import 'package:Snooze/locale/app_localization.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class Music extends StatefulWidget {
  @override
  _MusicState createState() => _MusicState();
}

class _MusicState extends State<Music> {
  Duration _duration = Duration();
  Duration _position = Duration();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();
    initPlayer();
  }

  void initPlayer() {
    advancedPlayer = AudioPlayer();
    audioCache = AudioCache(fixedPlayer: advancedPlayer);

    advancedPlayer.durationHandler = (d) {
      setState(() {
        _duration = d;
      });
    };

    advancedPlayer.positionHandler = (p) {
      setState(() {
        _position = p;
      });
    };
  }

  String localFilePath;

  /* Widget _tab(List<Widget> children) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children:
                children.map((w) => Container(child: w, padding: EdgeInsets.all(6.0))).toList(),
          ),
        ),
      ],
    );
  }*/

  Widget _btn(String txt, VoidCallback onPressed) {
    return ButtonTheme(
      minWidth: 48.0,
      child: Container(
        width: 150,
        height: 70,
        child: RaisedButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(35)),
            child: Text(
              txt,
              style: TextStyle(
                fontFamily: 'SourceSerifPro',
                fontSize: 25,
                fontWeight: FontWeight.w700,
              ),
            ),
            color: Colors.pink[900],
            textColor: Colors.white,
            onPressed: onPressed),
      ),
    );
  }

  Widget slider() {
    return Slider(
        activeColor: Colors.black,
        inactiveColor: Colors.pink,
        value: _position.inSeconds.toDouble(),
        min: 0.0,
        max: _duration.inSeconds.toDouble(),
        onChanged: (double value) {
          setState(() {
            seekToSecond(value.toInt());
            value = value;
          });
        });
  }

  Widget localAsset() {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _btn('Play', () => audioCache.play('assets/music/drivebymusik.mp3')),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _btn('Pause', () => advancedPlayer.pause()),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _btn('Stop', () => advancedPlayer.stop()),
              ),
            ],
          ),
        //  Padding(padding: EdgeInsets.all(10), child: slider())
        ],
      ),
    );
  }
  /*Widget localAsset() {
    return _tab([
      Text('Play Local Asset \'drivebymusik.mp3\':'),
      _btn('Play', () => audioCache.play('drivebymusik.mp3')),
      _btn('Pause', () => advancedPlayer.pause()),
      _btn('Stop', () => advancedPlayer.stop()),
      slider()
    ]);
  }*/

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);

    advancedPlayer.seek(newDuration);
  }

 
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff87D3ED),
      child: new Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, top: 30),
                    child: Text(
                      'Musik',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'SourceSerifPro',
                        fontSize: 40,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 2.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, top: 30),
                    child: Text(
                      'Wähle deine Musikwunsch',
                      style: TextStyle(
                        color: Colors.grey,
                        fontFamily: 'SourceSerifPro',
                        fontSize: 35,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 2.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  SizedBox(height: 30.0),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Wrap(
                      spacing: 20.0,
                      runSpacing: 20.0,
                      direction: Axis.horizontal,
                      children: <Widget>[
                        gewitter(),
                        regen(),
                        safari(),
                        starkerWind(),
                        wasserFall(),
                        dschungel(),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          new Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(80.0),
                child: localAsset(),
              ),
              /* new Expanded(
                child: new Text('BBBB'),
              ),
              new Expanded(
                child: new Text('BBBB'),
              ),*/
            ],
          ),
        ],
      ),
    );
  }
}

/*void onPlayAudio() async {
  AssetsAudioPlayer assetsAudioPlayer = AssetsAudioPlayer();
  assetsAudioPlayer.open(
    Audio('assets/music/drivebymusik.mp3'),
  );
}*/

Widget regen() {
  return Card(
    elevation: 3,
    child: InkWell(
      splashColor: Colors.orange,
      onTap: () {
      /*  onPlayAudio();*/
      },
      child: Container(
        height: 180,
        width: 180,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Center(
              child: Image.asset(
                'assets/images/regen.png',
                height: 120,
                width: 120,
              ),
            ),
            SizedBox(height: 10.0),
            Center(
              child: Text(
                'Regen',
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'SourceSerifPro',
                  letterSpacing: 2.0,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget gewitter() {
  return Card(
    elevation: 3,
    child: InkWell(
      splashColor: Colors.orange,
      onTap: () {},
      child: Container(
        height: 180,
        width: 180,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Center(
              child: Image.asset(
                'assets/images/gewitter.png',
                height: 120,
                width: 120,
              ),
            ),
            SizedBox(height: 10.0),
            Center(
              child: Text(
                'Gewitter',
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'SourceSerifPro',
                  letterSpacing: 2.0,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget safari() {
  return Card(
    elevation: 3,
    child: InkWell(
      splashColor: Colors.orange,
      onTap: () {},
      child: Container(
        height: 180,
        width: 180,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Center(
              child: Image.asset(
                'assets/images/AfrikaSafari.png',
                height: 120,
                width: 120,
              ),
            ),
            SizedBox(height: 10.0),
            Center(
              child: Text(
                'Safari',
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'SourceSerifPro',
                  letterSpacing: 2.0,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget starkerWind() {
  return Card(
    elevation: 3,
    child: InkWell(
      splashColor: Colors.orange,
      onTap: () {},
      child: Container(
        height: 180,
        width: 180,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Center(
              child: Image.asset(
                'assets/images/windLuft.png',
                height: 120,
                width: 120,
              ),
            ),
            SizedBox(height: 10.0),
            Center(
              child: Text(
                'Wind',
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'SourceSerifPro',
                  letterSpacing: 2.0,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget wasserFall() {
  return Card(
    elevation: 3,
    child: InkWell(
      splashColor: Colors.orange,
      onTap: () {},
      child: Container(
        height: 180,
        width: 180,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Center(
              child: Image.asset(
                'assets/images/wasserfall.png',
                height: 120,
                width: 120,
              ),
            ),
            SizedBox(height: 10.0),
            Center(
              child: Text(
                'Wasserfall',
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'SourceSerifPro',
                  letterSpacing: 2.0,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget dschungel() {
  return Card(
    elevation: 3,
    child: InkWell(
      splashColor: Colors.orange,
      onTap: () {},
      child: Container(
        height: 180,
        width: 180,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Center(
              child: Image.asset(
                'assets/images/dschungel.png',
                height: 120,
                width: 120,
              ),
            ),
            SizedBox(height: 10.0),
            Center(
              child: Text(
                'Wald',
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'SourceSerifPro',
                  letterSpacing: 2.0,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
/*
  Duration _duration = Duration();
  Duration _position = Duration();
  AudioPlayer advancedPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();
    initPlayer();
  }

  void initPlayer() {
    advancedPlayer = AudioPlayer();
    audioCache = AudioCache(fixedPlayer: advancedPlayer);

    advancedPlayer.durationHandler = (d) {
      setState(() {
        _duration = d;
      });
    };

    advancedPlayer.positionHandler = (p) {
      setState(() {
        _position = p;
      });
    };
  }

  String localFilePath;

  Widget _tab(List<Widget> children) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children:
                children.map((w) => Container(child: w, padding: EdgeInsets.all(6.0))).toList(),
          ),
        ),
      ],
    );
  }

  Widget _btn(String txt, VoidCallback onPressed) {
    return ButtonTheme(
      minWidth: 48.0,
      child: Container(
        width: 150,
        height: 45,
        child: RaisedButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            child: Text(txt),
            color: Colors.pink[900],
            textColor: Colors.white,
            onPressed: onPressed),
      ),
    );
  }

  Widget slider() {
    return Slider(
        activeColor: Colors.black,
        inactiveColor: Colors.pink,
        value: _position.inSeconds.toDouble(),
        min: 0.0,
        max: _duration.inSeconds.toDouble(),
        onChanged: (double value) {
          setState(() {
            seekToSecond(value.toInt());
            value = value;
          });
        });
  }

  Widget localAsset() {
    return _tab([
      Text('Play Local Asset \'drivebymusik.mp3\':'),
      _btn('Play', () => audioCache.play('drivebymusik.mp3')),
      _btn('Pause', () => advancedPlayer.pause()),
      _btn('Stop', () => advancedPlayer.stop()),
      slider()
    ]);
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);

    advancedPlayer.seek(newDuration);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 1.0,
          backgroundColor: Colors.teal,
          title: Center(child: Text('LOCAL AUDIO')),
        ),
        body: TabBarView(
          children: [localAsset()],
        ),
      ),
    );
  }
}
*/

/*
class Music extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.cyan[100],
      child: new Center(
        child: Text('Index 3: '+AppLocalization.of(context).music),
      ),
    );
  }
}*/
